module Main exposing (main)

import Browser exposing (Document, application)
import Browser.Navigation as Navigation exposing (Key)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Json
import OAuth
import OAuth.AuthorizationCode
import Url exposing (Protocol(..), Url)


type alias Model =
    { redirectUri : Url
    , error : Maybe String
    , token : Maybe OAuth.Token
    , profile : Maybe Profile
    , state : String
    }


type alias Profile =
    { username : String
    }


type OAuthProvider
    = Cognito


type alias OAuthConfiguration =
    { provider : OAuthProvider
    , authorizationEndpoint : Url
    , tokenEndpoint : Url
    , profileEndpoint : Url
    , logoutEndpoint : Url
    , clientId : String
    , secret : String
    , scope : List String
    , profileDecoder : Json.Decoder Profile
    }


main : Program { randomBytes : String } Model Msg
main =
    application
        { init = init
        , view = view
        , update = update
        , subscriptions = always Sub.none
        , onUrlRequest = always NoOp
        , onUrlChange = always NoOp
        }


view : Model -> Document Msg
view model =
    let
        content =
            case ( model.token, model.profile ) of
                ( Nothing, Nothing ) ->
                    [ button [ onClick (SignInRequested <| configurationFor Cognito) ] [ text "Sign in" ]
                    ]

                ( Just token, Nothing ) ->
                    [ div [] [ text "fetching profile..." ] ]

                ( _, Just profile ) ->
                    [ div [] [ text profile.username ]
                    , div [] [ button [ onClick (SignOutRequested <| configurationFor Cognito) ] [ text "Sign Out" ] ]
                    ]
    in
    { title = "Elm OAuth2 Example - AuthorizationCode Flow"
    , body = [ div [] (viewError model.error :: content) ]
    }


viewError : Maybe String -> Html msg
viewError error =
    case error of
        Nothing ->
            div [ style "display" "none" ] []

        Just msg ->
            div [] [ text msg ]



--
-- Msg
--


type
    Msg
    -- No Operation, terminal case
    = NoOp
      -- The 'sign-in' button has been hit
    | SignInRequested OAuthConfiguration
      -- The 'sign-out' button has been hit
    | SignOutRequested OAuthConfiguration
      -- Got a response from the googleapis token endpoint
    | GotAccessToken OAuthConfiguration (Result Http.Error OAuth.AuthorizationCode.AuthenticationSuccess)
      -- Got a response from the googleapis info endpoint
    | GotUserInfo (Result Http.Error Profile)


getUserInfo : OAuthConfiguration -> OAuth.Token -> Cmd Msg
getUserInfo { profileEndpoint, profileDecoder } token =
    Http.send GotUserInfo <|
        Http.request
            { method = "GET"
            , body = Http.emptyBody
            , headers = OAuth.useToken token []
            , withCredentials = False
            , url = Url.toString profileEndpoint
            , expect = Http.expectJson profileDecoder
            , timeout = Nothing
            }


getAccessToken : OAuthConfiguration -> Url -> String -> Cmd Msg
getAccessToken ({ clientId, secret, tokenEndpoint } as config) redirectUri code =
    Http.send (GotAccessToken config) <|
        Http.request <|
            OAuth.AuthorizationCode.makeTokenRequest
                { credentials =
                    { clientId = clientId
                    , secret = Just secret
                    }
                , code = code
                , url = tokenEndpoint
                , redirectUri = redirectUri
                }



--
-- Init
--


init : { randomBytes : String } -> Url -> Key -> ( Model, Cmd Msg )
init { randomBytes } origin _ =
    let
        model =
            { redirectUri = { origin | query = Nothing, fragment = Nothing }
            , error = Nothing
            , token = Nothing
            , profile = Nothing
            , state = randomBytes
            }
    in
    case OAuth.AuthorizationCode.parseCode origin of
        OAuth.AuthorizationCode.Success { code, state } ->
            if Maybe.map randomBytesFromState state /= Just model.state then
                ( { model | error = Just "'state' doesn't match, the request has likely been forged by an adversary!" }
                , Cmd.none
                )

            else
                case Maybe.andThen (Maybe.map configurationFor << oauthProviderFromState) state of
                    Nothing ->
                        ( { model | error = Just "Couldn't recover OAuthProvider from state" }
                        , Cmd.none
                        )

                    Just config ->
                        ( model
                        , getAccessToken config model.redirectUri code
                        )

        OAuth.AuthorizationCode.Empty ->
            ( model, Cmd.none )

        OAuth.AuthorizationCode.Error err ->
            ( { model | error = Just (OAuth.errorCodeToString err.error) }
            , Cmd.none
            )



--
-- Update
--


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        SignInRequested { scope, provider, clientId, authorizationEndpoint } ->
            let
                auth =
                    { clientId = clientId
                    , redirectUri = model.redirectUri
                    , scope = scope
                    , state = Just (makeState model.state provider)
                    , url = authorizationEndpoint
                    }
            in
            ( model
            , auth |> OAuth.AuthorizationCode.makeAuthUrl |> Url.toString |> Navigation.load
            )

        SignOutRequested { scope, provider, clientId, logoutEndpoint } ->
            let
                auth =
                    { clientId = clientId
                    , redirectUri = model.redirectUri
                    , scope = scope
                    , state = Just (makeState model.state provider)
                    , url = logoutEndpoint
                    }
            in
            ( model
            , auth |> OAuth.AuthorizationCode.makeAuthUrl |> Url.toString |> Navigation.load
            )

        GotAccessToken config res ->
            case res of
                Err (Http.BadStatus { body }) ->
                    case Json.decodeString OAuth.AuthorizationCode.defaultAuthenticationErrorDecoder body of
                        Ok { error, errorDescription } ->
                            let
                                errMsg =
                                    "Unable to retrieve token: " ++ errorResponseToString { error = error, errorDescription = errorDescription }
                            in
                            ( { model | error = Just errMsg }
                            , Cmd.none
                            )

                        _ ->
                            ( { model | error = Just ("Unable to retrieve token: " ++ body) }
                            , Cmd.none
                            )

                Err _ ->
                    ( { model | error = Just "Unable to retrieve token: HTTP request failed. CORS is likely disabled on the authorization server." }
                    , Cmd.none
                    )

                Ok { token } ->
                    ( { model | token = Just token }
                    , getUserInfo config token
                    )

        GotUserInfo res ->
            case res of
                Err _ ->
                    ( { model | error = Just "Unable to retrieve user profile: HTTP request failed." }
                    , Cmd.none
                    )

                Ok profile ->
                    Debug.log "model"
                        ( { model | profile = Just profile }
                        , Cmd.none
                        )



--
-- Helpers
--


errorResponseToString : { error : OAuth.ErrorCode, errorDescription : Maybe String } -> String
errorResponseToString { error, errorDescription } =
    let
        code =
            OAuth.errorCodeToString error

        desc =
            errorDescription
                |> Maybe.withDefault ""
                |> String.replace "+" " "
    in
    code ++ ": " ++ desc


oauthProviderToString : OAuthProvider -> String
oauthProviderToString provider =
    case provider of
        Cognito ->
            "cognito"


oauthProviderFromString : String -> Maybe OAuthProvider
oauthProviderFromString str =
    case str of
        "cognito" ->
            Just Cognito

        _ ->
            Nothing


makeState : String -> OAuthProvider -> String
makeState suffix provider =
    oauthProviderToString provider ++ "." ++ suffix


oauthProviderFromState : String -> Maybe OAuthProvider
oauthProviderFromState str =
    str
        |> stringLeftUntil (\c -> c == ".")
        |> oauthProviderFromString


randomBytesFromState : String -> String
randomBytesFromState str =
    str
        |> stringDropLeftUntil (\c -> c == ".")


stringDropLeftUntil : (String -> Bool) -> String -> String
stringDropLeftUntil predicate str =
    let
        ( h, q ) =
            ( String.left 1 str, String.dropLeft 1 str )
    in
    if q == "" || predicate h then
        q

    else
        stringDropLeftUntil predicate q


stringLeftUntil : (String -> Bool) -> String -> String
stringLeftUntil predicate str =
    let
        ( h, q ) =
            ( String.left 1 str, String.dropLeft 1 str )
    in
    if h == "" || predicate h then
        ""

    else
        h ++ stringLeftUntil predicate q


configurationFor : OAuthProvider -> OAuthConfiguration
configurationFor provider =
    let
        defaultHttpsUrl =
            { protocol = Https
            , host = ""
            , path = ""
            , port_ = Nothing
            , query = Nothing
            , fragment = Nothing
            }
    in
    case provider of
        Cognito ->
            { provider = Cognito
            , clientId = "3gibpvsicj5tqecsvrhqqoj6o0"
            , secret = "aq7a4t4hbi7udq7im6b6023b8l314dj2sl7bo4bvdbfsuj67nhm"
            , authorizationEndpoint = { defaultHttpsUrl | host = "mytesto.auth.us-east-1.amazoncognito.com", path = "/oauth2/authorize" }
            , tokenEndpoint = { defaultHttpsUrl | host = "mytesto.auth.us-east-1.amazoncognito.com", path = "/oauth2/token" }
            , profileEndpoint = { defaultHttpsUrl | host = "mytesto.auth.us-east-1.amazoncognito.com", path = "/oauth2/userInfo" }
            , logoutEndpoint = { defaultHttpsUrl | host = "mytesto.auth.us-east-1.amazoncognito.com", path = "/logout" }
            , scope = []
            , profileDecoder =
                Json.map Profile
                    (Json.field "username" Json.string)
            }
